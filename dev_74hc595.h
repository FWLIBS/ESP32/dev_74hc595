/* SHIFT REGISTER 74HC595

   dev: duandh204
   email: duandh204@gmail.com

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#ifndef _DEV_74HC595_H_
#define _DEV_74HC595_H_

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/gpio.h"

// Define GPIO ==> board.h
//#define SDAT 3
//#define SCLK 4
//#define SLCH 5

void hc595_init(void);
void hc595_shift(unsigned char _data);
void hc595_clock(void);
void hc595_latch(void);

#endif  
