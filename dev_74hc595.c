/* SHIFT REGISTER 74HC595

   dev: duandh204
   email: duandh204@gmail.com

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include "dev_74hc595.h"


void hc595_init(void)
{
   gpio_reset_pin(SDAT);
   gpio_set_direction(SDAT, GPIO_MODE_OUTPUT);

   gpio_reset_pin(SCLK);
   gpio_set_direction(SCLK, GPIO_MODE_OUTPUT);

   gpio_reset_pin(SLCH);
   gpio_set_direction(SLCH, GPIO_MODE_OUTPUT);

}

void hc595_shift(unsigned char s_data)
{
   unsigned char i, temp;
   temp = s_data;
   for(i = 0; i<8; i++)
   {
      if(temp & 0x80)
         gpio_set_level(SDAT, 1); // SDAT = 1
      else
         gpio_set_level(SDAT, 0); // SDAT = 0
      
      hc595_clock();
      temp = temp << 1;
  }
}

void hc595_clock(void)
{
   gpio_set_level(SCLK, 0); // SCLK = 1
   //vTaskDelay(1 / portTICK_PERIOD_MS);
   __asm__ __volatile__ ("nop");
   __asm__ __volatile__ ("nop");
   gpio_set_level(SCLK, 1); // SCLK = 0
   __asm__ __volatile__ ("nop");   
}

void hc595_latch(void)
{
   gpio_set_level(SLCH, 0); // SLCH = 1
   //vTaskDelay(1 / portTICK_PERIOD_MS);
   __asm__ __volatile__ ("nop");
   __asm__ __volatile__ ("nop");
   gpio_set_level(SLCH, 1); // SLCH = 0
   __asm__ __volatile__ ("nop");
}
